#!/bin/sh

set -e

__print_break() {
  if [ -z "$COLUMNS" ]; then
    if
      command -v tput >/dev/null 2>&1
    then
      COLUMNS=$(tput cols)
    else
      COLUMNS=40
    fi
  fi

  # Create a line of dashes equal to the number of columns
  printf '%*s\n' "$COLUMNS" | tr ' ' '-'
}

echo "Installing server..."
__print_break
java -jar forge-installer.jar --installServer ./server/
