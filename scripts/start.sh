#!/bin/sh

set -e

__print_break() {
  if [ -z "$COLUMNS" ]; then
    if
      command -v tput >/dev/null 2>&1
    then
      COLUMNS=$(tput cols)
    else
      COLUMNS=40
    fi
  fi

  # Create a line of dashes equal to the number of columns
  printf '%*s\n' "$COLUMNS" | tr ' ' '-'
}

# Run installer
./installer.sh

# Setup envs & vars
current_dir=$PWD
xmx=${JAVA_XMX:-1024m} # Minimum requirement is 1G
xms=${JAVA_XMS:-256m}
options=${JAVA_OPTS:-}

# Switch Directory
cd ${current_dir}/server

# Override the default JVM options
echo "-Xms${xms} -Xmx${xmx}" >${current_dir}/user_jvm_args.txt

# Run the server
__print_break
echo "Starting the server..."
__print_break
./run.sh --nogui $@
