#!/bin/sh

set -e

# Step 1: Download the JSON data
json_data=$(curl -Ls https://files.minecraftforge.net/maven/net/minecraftforge/forge/promotions_slim.json)

# Step 2: Extract the latest Minecraft version and its corresponding Forge version
if [ -n "$1" ]; then
  latest_mc_version="$1"
elif [ -n "$MC_VERSION" ]; then
  latest_mc_version="$MC_VERSION"
else
  echo "Minecraft version is not specified, getting the latest one..."
  latest_mc_version=$(echo "$json_data" | jq -r '.promos | to_entries | map(select(.key | test("-latest$"))) | sort_by(.key | split("-")[0] | split(".") | map(try tonumber catch 0)) | last | .key | split("-")[0]')
fi
latest_forge_version=$(echo "$json_data" | jq -r --arg version "$latest_mc_version-latest" '.promos[$version]')

# Step 3: Construct the URL for the Forge installer
installer_url="https://files.minecraftforge.net/maven/net/minecraftforge/forge/$latest_mc_version-$latest_forge_version/forge-$latest_mc_version-$latest_forge_version-installer.jar"

if [ -z "$latest_forge_version" ] || [ "$latest_forge_version" = "null" ]; then
  echo "Failed to retrieve Minecraft version $latest_mc_version"
  echo "Available Minecraft versions: $json_data"
  exit 1
else
  # Print the versions and URL for verification
  echo "Minecraft Version: $latest_mc_version"
  echo "Latest Forge Version: $latest_forge_version"
  echo "Installer URL: $installer_url"

  # Step 4: Download the Forge installer
  curl -SL -o forge-installer.jar $installer_url
fi
