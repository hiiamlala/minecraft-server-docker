#!/bin/bash

set -e

build_job() {
  set -e
  version=$(echo "$1" | sed 's/-latest//')
  echo "Processing version: $version"
  sanitized_version=$(echo "$version" | sed 's/[^a-zA-Z0-9_.-]/_/g')
  docker_image=${DOCKER_REGISTRY_USER}/${DOCKER_REGISTRY_IMAGE}:${sanitized_version}
  docker build --no-cache -q --build-arg MC_VERSION=${version} -t ${docker_image} .
  echo "Built image: ${docker_image}"
  docker push -q ${docker_image}
  echo "Pushed image: ${docker_image}"
  docker system prune -f
}

export -f build_job

json_data=$(curl -Ls https://files.minecraftforge.net/maven/net/minecraftforge/forge/promotions_slim.json)

latest_mc_versions=$(echo "$json_data" | jq -r '.promos | to_entries | map(select(.key | test("-latest$"))) | map(.key)')

echo "$latest_mc_versions" | jq -r '.[]' | parallel -j 2 build_job {}

echo "All jobs are done."
