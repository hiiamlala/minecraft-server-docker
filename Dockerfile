FROM --platform=amd64 alpine:latest AS base

ARG MC_VERSION

FROM base AS getter

RUN apk add --no-cache curl jq \
  && rm -rf /var/cache/apk/*

WORKDIR /tmp

COPY scripts/getter.sh ./

RUN ./getter.sh

FROM base AS runner

RUN apk add --no-cache openjdk21-jdk \
  && rm -rf /var/cache/apk/*

ARG user=mcuser

RUN adduser -D -h /var/minecraft -u 1210 $user

WORKDIR /var/minecraft

COPY --chown=${user}:${user} --from=getter /tmp/forge-installer.jar ./

COPY --chown=${user}:${user} scripts/start.sh scripts/installer.sh ./

USER ${user}

EXPOSE 25565

ENTRYPOINT [ "./start.sh" ]